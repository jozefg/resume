Danny Gratzer
=================

jozefg@cmu.edu, 952-686-3386
<http://jozefg.bitbucket.org>

## Information

I'm an undergraduate student at CMU fascinated by types and
programming languages. I'm particularly interested in using modern
programming language research and functional languages to tackle real
and significant problems. I love type theory, math, and juggling.

## Education

* **BS** in Computer Science at Carnegie Mellon (2014 -)
* **PSEO** at the University of Minnesota (2012 - 2014)
* **High School** at Edina High School (2010 - 2014) (graduated)

## Work

* **Teaching Assistant** at Carnegie Mellon (Dec. 2015 -)

    I am currently a teaching assistant at Carnegie Mellon. I
    previously TAed 15-150 (Functional Programming) for two
    semesters. In this class I introduced undergraduates to functional
    programming and basic ideas of formal proofs about
    programs. Currently I am a TA for 15-312 (Foundations of
    Programming Languages) where we broach the topic of using
    mathematical tools such as type theory and logic to investigate the
    formal properties of programming languages.

* **Research Assistant** at Carnegie Mellon (Sept. 2014 -)

    I am currently working under Karl Crary in programming languages
    and type theory. Specifically, we're investigating tools for
    reasoning about and formally proving the correctness of programs
    in languages with advanced features like general state. The
    eventual goal in our research is to build a proof assistant which
    truly incorporates the ability to prove the correctness of such
    program. Prior to this we investigated a type theory which incorporates
    both the rich computational ideas of type theories like Coq or
    Agda along with the ability to support "higher order abstract
    syntax" ala Twelf.
* **Summer Intern** at CSAIL at MIT (May 2013 - Sept. 2013)

    I worked Adam Chlipala building and verifying a
    compiler for a simple functional language to a variant of C. I
    both built and verified this compiler using Coq. This was part of
    the Bedrock project which aims to provide a full platform from
    assembly up amenable to formal verification.
* **Research Assistant** at the University of Minnesota
  (Sept. 2013 - May 2013)

    I worked under John Riedl and Michael Ekstrand on Lenskit, a
    hotbed for building and testing recommendation algorithms. I
    integrated the set of algorithms built for GraphChi with Lenskit
    in lenskit-graphchi as well as making several changes to Lenskit
    proper.

## Open Source

I have a lot of open source projects. Most of these can be found at
<http://bitbucket.org/jozefg> and <http://github.com/jozefg>. Some
highlights include

- **miniprl** - a semi-literate implementation of a proof refinement logic/proof assistant
- **pcf** - a compiler for a statically typed functional language
- **folds-common** - a standard library of constant space stream-processors in Haskell
- **hasquito** - a compiler for a lazy functional language
- **c_of_scheme** - a compiler for Scheme
- **hlf** - an implemention of a Twelf-like dependently typed language

I am also a major contributor the JonPRL (<http://github.com/jonprl>
and <http://github.com/jonsterling/jonprl>) proof
assistant and have heavily contributed to that as well as the half a
dozen satellite projects around its main implementation.

Additionally, I run a blog about Haskell and programming languages at
<http://jozefg.bitbucket.org>.

Skills
-------

I am very experienced with a number of functional languages such
as Haskell, SML, OCaml, Scheme. Additionally I am very familiar with C
and Python.

Outside of traditional program languages, I'm also familiar with most
common proof assistants though I have the most experience with Coq,
Agda, and Twelf. As far as other technology-related experience, I'm
familiar with Emacs and git. I predominately use Linux for
development.
